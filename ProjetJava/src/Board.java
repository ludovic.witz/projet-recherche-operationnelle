import java.util.ArrayList;
import java.util.Iterator;

public class Board {
	
	ArrayList<ArrayList<Cell>> _cells;
	int _size;
	ArrayList<Path> _paths;
	Path _path;
	
	public static void main(String[] args) {
		try {
			
			Board board = new Board(6);
			board.solve();
			
		} catch (Exception e) {//IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private Cell getCell(int line, int column) {
		Cell cell = _cells.get(line).get(column);
		return cell;
	}

	public Board(int size) {
		_size = size;
		_cells = new ArrayList<ArrayList<Cell>>();
		_paths = new ArrayList<Path>();
		for (int i=0; i<_size; i++) {
			ArrayList<Cell> lineCells = new ArrayList<Cell>(); 
			for (int j=0; j<_size; j++) {
				Cell cell = new Cell(i, j);
				lineCells.add(cell);
			}
			_cells.add(lineCells);
		}
		computeLinkedCells();
	}
	
	private void computeLinkedCells() {
		for (ArrayList<Cell> lineCells:_cells) {
			for (Cell cell:lineCells) {
				addCellIfInBoard(cell,cell._x-2, cell._y+1);
				addCellIfInBoard(cell,cell._x-2, cell._y-1);
				addCellIfInBoard(cell,cell._x+2, cell._y+1);
				addCellIfInBoard(cell,cell._x+2, cell._y-1);
				addCellIfInBoard(cell,cell._x-1, cell._y+2);
				addCellIfInBoard(cell,cell._x-1, cell._y-2);
				addCellIfInBoard(cell,cell._x+1, cell._y+2);
				addCellIfInBoard(cell,cell._x+1, cell._y-2);
			}
		}
	}

	private void addCellIfInBoard(Cell cell, int i, int j) {
		if (checkIfCellInBoard(i, j)) {
			cell._possibleCells.add(_cells.get(i).get(j));
		}
	}

	private void solve() {
		Cell cellStart = getCell(0,0);
		_path = new Path(this, null, cellStart);
		if (explorePath(_path)) {
			System.out.println(_path.toString());
			System.out.println(_path._length);
		}
		else {
			System.out.println("Aucun chemin trouvé !!");
		}
	}

	private boolean isSolved(Path path) {
		if (!areLinked(path._newCell, path._startCell)){
			return false;
		}
		for (ArrayList<Cell> lineCells:_cells) {
			for (Cell cell:lineCells) {
				if (!isVisited(path, cell)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean explorePath(Path path) {
		for (Cell possibleCell: path._newCell._possibleCells){
			if (!isVisited(path, possibleCell)) {
				Path new_path = new Path(this, path, possibleCell);
				if (isSolved(new_path)) {
					_path = new_path;
					return true;
				}
				if (explorePath(new_path)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean areLinked(Cell cellFrom, Cell cellTo) {
		return cellFrom._possibleCells.contains(cellTo);
	}
	
	private boolean isVisited(Path path, Cell cell) {
		int x = cell._x;
		int y = cell._y;
		return path._visited[x][y];
	}

	private boolean checkIfCellInBoard(int x, int y) {
		return (0 <= x && x < this._size) && (0 <= y && y < this._size);
	}

}