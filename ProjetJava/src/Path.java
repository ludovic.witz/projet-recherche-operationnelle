public class Path {
	
	Board _board;
	Path _pathPred;
	Cell _startCell;
	Cell _newCell;
	boolean[][] _visited;
	int _length;
	
	public Path(Board board, Path pathPred, Cell cell) {
		_board = board;
		_pathPred = pathPred;
		_newCell = cell;
		if (pathPred==null) {
			_length = 1;
		} else {
			_length = pathPred._length+1;
		}
		int size = _board._size;
		_visited = new boolean[size][size];
		for (int i=0; i<size; i++) {
			for (int j=0; j<_board._size; j++) {
				if (pathPred==null) {
					_visited[i][j] = false;
					_startCell = cell;
				} else {
					_visited[i][j] = _pathPred._visited[i][j];
					_startCell = _pathPred._startCell;
				}
			}
		}
		int x = cell._x;
		int y = cell._y;
		_visited[x][y] = true;
	}
	
	@Override
	public String toString() {
		String strPath = "";
		if (_pathPred != null) {
			strPath += _pathPred.toString() + " - " ;
		} else {
			strPath += "Path = ";
		}
		strPath += _newCell.toString();
		return strPath; 
	}
	
}