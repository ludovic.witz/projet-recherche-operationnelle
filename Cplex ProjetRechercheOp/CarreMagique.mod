/*********************************************
 * OPL 20.1.0.0 Model
 * Author: Pierre Hebinger et Ludovic Witz
 * Creation Date: 11 f�vr. 2021 at 16:29:25
 *********************************************/

 
 using CP;
 
 int N = 6;
 
 
 range cote = 1..N;
 
 range rangeXXX = 1..(N*N);
 dvar int carre[cote][cote] in rangeXXX;
 
 float sommeMagique = N * (( N*N + 1 ) /2);
 


 maximize carre[5][1] + carre[6][2] + carre[2][3] + carre[4][4] + carre[1][5] + carre[3][6];
 
 
 subject to {
   
   allDifferent(carre);
   
   forall(i in cote) (sum(c in cote) carre[i][c] == sommeMagique); //vertical
   forall(i in cote) (sum(l in cote) carre[l][i] == sommeMagique); //horizontal
   sum(i in cote) carre[i][i] == sommeMagique; //diagonal gauche vers droite
   sum(i in cote) carre[N - i + 1][i] == sommeMagique; //diagonal droite vers gauche
 }